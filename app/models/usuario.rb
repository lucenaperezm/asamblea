class Usuario < ActiveRecord::Base
  attr_accessible :ci, :condicion_id, :credencial, :distrito_id, :nombre, :region_id
  validates_presence_of :ci, :condicion_id, :credencial, :distrito_id, :nombre, :region_id
  validates_uniqueness_of :ci, :credencial
  belongs_to :region
  belongs_to :distrito
  belongs_to :condicion
end
