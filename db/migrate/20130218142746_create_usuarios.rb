class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.text :nombre
      t.text :ci
      t.text :credencial
      t.integer :region_id
      t.integer :distrito_id
      t.integer :condicion_id

      t.timestamps
    end
  end
end
